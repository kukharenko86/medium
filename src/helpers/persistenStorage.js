export const getItem =key =>{
  try {
    return JSON.parse(localStorage.getItem(key))
  } catch (error) {
    console.log('getting data from local storage', error)
  }
  return null
}

export const setItem =(key,data) =>{
  try {
    return localStorage.setItem(key, JSON.stringify(data))
  } catch (error) {
    console.log('setting data from local storage', error)
  }
}
