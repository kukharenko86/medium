import authApi from '@/api/auth'
import { setItem } from "@/helpers/persistenStorage";
const state =   {
    isSubmiting: false,
    currentUser: null,
    validationErrors: null,
    isLoggedIn: null
}

export const mytationTypes = {
  registerStart: '[auth] registerStart',
  registerSucces: '[auth] registerSucces',
  registerFailure: '[auth] registerFailure',
  loginStart: '[auth] loginStart',
  loginSucces: '[auth] loginSucces',
  loginFailure: '[auth] loginFailure',
}
export const actionTypes = {
  register: '[auth] register',
  login: '[auth] login',
}

const mutations = {
    [mytationTypes.registerStart](state){
        state.isSubmiting = true;
        state.validationErrors = null;
    },
    [mytationTypes.registerSucces](state, payload){
      state.isSubmiting = false;
      state.currentUser = payload;
      state.isLoggedIn = true;

  },
  [mytationTypes.registerFailure](state,payload){
    state.isSubmiting = false;
    state.validationErrors = payload;
},
[mytationTypes.loginStart](state){
  state.isSubmiting = true;
  state.validationErrors = null;
},
[mytationTypes.loginSucces](state, payload){
state.isSubmiting = false;
state.currentUser = payload;
state.isLoggedIn = true;

},
[mytationTypes.loginFailure](state,payload){
state.isSubmiting = false;
state.validationErrors = payload;
},
}


const actions ={
  [actionTypes.register](context, credentials){
    return new Promise(resolve => {
      context.commit(mytationTypes.registerStart)
      authApi.register(credentials)
      .then(response => {
       context.commit(mytationTypes.registerSucces,response.data.user)
       setItem('accessToken', response.data.user.token)
       resolve(response.data.user)
      })
      .catch(result =>{
        context.commit(mytationTypes.registerFailure,result.response.data.errors)
        console.log('Ошибочка', result)
      })
    })
  },
  [actionTypes.login](context, credentials){
    return new Promise(resolve => {
      context.commit(mytationTypes.loginStart)
      authApi.login(credentials)
      .then(response => {
       context.commit(mytationTypes.loginSucces,response.data.user)
       setItem('accessToken', response.data.user.token)
       resolve(response.data.user)
      })
      .catch(result =>{
        context.commit(mytationTypes.loginFailure,result.response.data.errors)
        console.log('Ошибочка', result)
      })
    })
  }
}
export default {
    state,
    mutations,
    actions
}
